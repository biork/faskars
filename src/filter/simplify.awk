
# 1. Remove all contigs except the canonical chromosomes (1-22,X,Y) from the
# 	GRCh38 genome downloaded from NCBI, and
# 2. simplify contig headers to contain only the canonical chromosome name.

BEGIN {
	emitting = 0
}

/^>/ {
	if( match($0,/^>NC_0+([1-9]+0?)\./,a) == 1 ) {
		if( a[1] == 23 ) {
			printf(">X\n" )
		} else 
		if( a[1] == 24 ) {
			printf(">Y\n" )
		} else 
		if( a[1] == 12920 ) {
			printf(">M\n" )
		} else {
			printf(">%s\n", a[1])
		}
		emitting = 1
	} else {
		emitting = 0
	}
	next
}

emitting > 0 {
	print
}

