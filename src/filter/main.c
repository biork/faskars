
/**
 * "Simplify" means convert all sequence characters not in {A,C,G,T} to N.
 * In particular, IUPAC nomenclature is lost.
 * FASTA headers are untouched.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <getopt.h>

static int _process( FILE *fp_in, bool verbose, FILE *fp_out ) {

	uint64_t nbytes = 0;
	uint64_t changes = 0;
	bool header = false;
	int c;

	while( true ) {

		c = getc( fp_in );

		if( c == EOF )
			break;

		nbytes += 1; // Count explicitly since we can't use ftell on stdin.

		if( header ) {

			if( '\n' == c /*|| '\r' == c*/ ) { // CR/LF is normal sequence.
				header = false;
			}

		} else {

			if( strchr("ACGNT>\n",toupper(c)) ) {
				header = ( '>' == c );
			} else {
				if( verbose ) {
					fprintf( stderr, "%c->N\t%ld\n", c, nbytes );
				}
				changes ++;
				c = 'N';
			}
		}

		fputc(  c , fp_out );
	}

	fprintf( stderr,
		"%ld bytes processed\n"
		"%ld changes\n",
		nbytes, changes );

	return 0;
}

static bool arg_verbose = false;

static const char *USAGE =
"%s [ options ] < input.fa > output.fa\n"
"--verbose  | -v   report each change on stderr [%s]\n"
"    --help | -?   show this message\n"
#ifdef _DEBUG
"This is a debug build.\n"
#endif
"\n";

static void _print_usage( const char *exename, FILE *fp ) {
	fprintf( fp, USAGE,
		exename,
		arg_verbose ? "yes" : "no" );
}

int main( int argc, char *argv[] ) {
	do {
		static const char *CHAR_OPTIONS = "v?";
		static struct option LONG_OPTIONS[] = {
			{"verbose",  0,0,'v'},
			{"help",     0,0,'?'},
			{ NULL,      0,0, 0 }
		};
		int opt_index;
		const int c = getopt_long( argc, argv, CHAR_OPTIONS, LONG_OPTIONS, &opt_index );
		if( -1 == c ) break;
		switch( c ) {
		case 'v':
			arg_verbose = true;
			break;

		case '?':
			_print_usage( argv[0], stdout );
			exit(0);
		default:
			break;
		}
	} while(true);

	return _process( stdin, arg_verbose, stdout ) ? EXIT_FAILURE : EXIT_SUCCESS;
}

