
#wget ftp://ftp.ncbi.nlm.nih.gov/refseq/H_sapiens/annotation/GRCh38_latest/refseq_identifiers/GRCh38_latest_dbSNP_all.vcf.gz
#wget ftp://ftp.ncbi.nlm.nih.gov/refseq/H_sapiens/annotation/GRCh38_latest/refseq_identifiers/GRCh38_latest_genomic.fna.gz
#wget ftp://ftp.ncbi.nlm.nih.gov/refseq/H_sapiens/annotation/GRCh38_latest/refseq_identifiers/GRCh38_latest_genomic.gff.gz

# Following inference of executable directory works as long as this script
# is not sourced into login shell. TODO: Should detect/prevent that.
PROJECT=$(dirname $0)

IFASTA=${1:?'input FASTA genome file'}
OFASTA=${2:?'output FASTA genome file'}

cat ${IFASTA} | awk -f ${PROJECT}/simplify.awk | ${PROJECT}/fasta-simplify > ${OFASTA}
grep -v '^>' ${OFASTA} | ${PROJECT}/fasta-histogram | tee /tmp/histo.tab 

