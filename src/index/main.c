
/**
  * This program creates a 3-column table describing a multi-sequence
  * FASTA file. The columns contain:
  * 1. sequence headers copied verbatim from the FASTA file
  * 2. the file offset of the start of the header
  * 3. the file offset of the start of the sequence
  *
  * Note that this must be run on files since std I/O is not seekable.
  *
  * Running a script like:
  * ./indexlandmarks mouse.rna.fna > .starts
  * cut -f 2 .starts | tail -n +2 > .ends
  * stat -c '%s' mouse.rna.fna >> .ends
  * paste z .ends > mouse.rna.idx 
  * rm .starts .ends
  * ...transforms the result into a 3-column table in which the 2nd and
  * 3rd columns will be the (0-based, half-open) range of file offsets
  * containing the given sequence.
  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main( int argc, char *argv[] ) {

	int exit_status = EXIT_SUCCESS;

	for(int i = 1; i < argc; i++ ) {

		const char *FNAME = argv[1];
		FILE *fp = fopen( FNAME, "r" );

		if( fp ) {

			int  emit = 0;
			long hdr_off = -1; // hdr_off >= 0 implies we "in" a header.
			long cur_off = ftell(fp);
			int  cur_chr = fgetc( fp );
			if( 1 ) {
				printf( "@%s\n", FNAME );
			}

			while( 0 < cur_chr ) {

				if( cur_chr == '\r' ) {
					fprintf( stderr, "%s contains CR (\\r) pollution."
						"Fix it by running it through tr -d '\\r' 1st.\n", FNAME );
					exit_status = EXIT_FAILURE;
					i = argc;
					break;
				}

				if( cur_chr == '>' ) {
					emit = 1;
					hdr_off = cur_off; // We've entered a header.
				} else
				if( cur_chr == '\n' ) {
					if( hdr_off >= 0 ) {
						printf( "\t%ld\t%ld\n",
							hdr_off, ftell(fp) /* start of sequence data as long
								as file is free of \r pollution */ );
						hdr_off = -1;  // We've exited a header.
					}
					emit = 0;
				}

				if( emit ) {

					// Implicitly change TABs in header to SPC. This should
					// rarely if ever be necessary, but the ONLY TABs
					// emitted should be those above.
					if( cur_chr == '\t' ) {
						static int user_warned = 0;
						if( ! user_warned ) {
							fprintf( stderr, "warning: TABs changed to SPC in headers.\n" );
							user_warned += 1;
						}
						cur_chr = ' ';
					}
					fputc( cur_chr, stdout );
				}

				cur_off = ftell( fp );
				cur_chr = fgetc( fp );
			}

			fclose( fp );

		} else {

			fprintf( stderr, "failed opening %s: %s\n", FNAME, strerror(errno) );
			exit_status = EXIT_FAILURE;
			break;
		}
	}
	return exit_status;
}

