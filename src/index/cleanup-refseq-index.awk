BEGIN {
	FS="\t"
}
# Example input:
# @p/human.10.rna.fna
# >gi|1034650789|ref|XM_011535938.2| PREDICTED: Homo sapiens ER membrane-associated RNA degradation (ERMARD), transcript variant X1, mRNA	0	136
# >gi|767943121|ref|XR_942506.1| PREDICTED: Homo sapiens ER membrane-associated RNA degradation (ERMARD), transcript variant X2, misc_RNA	2276	2412
# >gi|1034650801|ref|XM_011535940.2| PREDICTED: Homo sapiens ER membrane-associated RNA degradation (ERMARD), transcript variant X9, mRNA	19957	20093

# Strip relative paths from filenames
/^@/ {
	if( match( $0, /^@.*\// ) > 0 ) {
		printf( "@%s\n", substr($0,RLENGTH+1) )
	} else {
		print
	}
	next
}

# Pull RefSeq accessions out of FASTA headers.
/^>/ {
	start = match( $1, /[XN][MR]_[0-9]+\.[0-9]+/ )
	if( start == 0 ) {
		printf( "No RefSeq accession found on line %d\n", NR )
		exit
	}
	printf( "%s\t%d\t%d\n", substr( $1, start, RLENGTH ), $2, $3 )
	next
}

{
	printf( "Unexpected prefix on line %d\n", NR )
	exit
}

