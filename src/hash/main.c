
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <getopt.h>

#include <openssl/md5.h>

#define SEP_CHAR        ('\t')
#define SIZEOF_HASHBUFF (4096)

static const char *_allowed = "";
static       char _separator = SEP_CHAR;
static bool _ignore_case = false;

static void _emit_length_sep_digest( const uint8_t *digest, int len, uint32_t seqlen, FILE *fp ) {
	static const char *_HEX = "0123456789ABCDEF";
	fprintf( fp, "%d%c", seqlen, _separator );
	while( len-- > 0 ) {
		const uint8_t byte = *digest++;
		fputc( _HEX[ (byte >> 4) & 0x0F ], fp );
		fputc( _HEX[ (byte     ) & 0x0F ], fp );
	}
	fputc( '\n', fp );
}

static int _process( FILE *fp_in, const char *allowed, bool ignore_case, FILE *fp_out ) {

	static uint8_t hashbuff[ SIZEOF_HASHBUFF ];
	static uint8_t digest[ MD5_DIGEST_LENGTH ];
	MD5_CTX ctx;
	uint32_t buflen = 0;
	uint32_t seqlen = 0;    // controls when an EMISSION occurs.
	uint64_t nbytes = 0;
	uint32_t seq_count = 0; // only incremented when an EMISSION occurs.

	bool header = false;
	while( true ) {

		const int c = getc( fp_in );
		if( c == EOF )
			break;

		nbytes += 1; // Count explicitly since we can't use ftell on stdin.

		if( header ) {

			if( '\n' == c || '\r' == c ) {
				fputc( _separator, fp_out );
				header = false;
				MD5_Init( &ctx );
			} else {
				if( c == _separator ) {
					fprintf( stderr, "Separator (0x%02x) found in header.\n"
						"Rerun using --sep arg to specify a different separator\n", c );
					abort();
				}
				fputc( c, fp_out ); // ...echo header chars verbatim.
			}

		} else {

			if( '>' == c ) {

				// If hash buffer is non-empty flush it.
				if( buflen > 0 ) {
					MD5_Update( &ctx, hashbuff, buflen );
					buflen = 0;
				}

				if( seqlen > 0 /* nothing to emit on 1st sequence */ ) {
					MD5_Final( digest, &ctx );
					_emit_length_sep_digest( digest, MD5_DIGEST_LENGTH, seqlen, fp_out );
					seqlen = 0;
					seq_count += 1;
				}
				header = true;

			} else
			if( ! isspace(c) ) {

				const uint8_t CONVCHAR
					= ignore_case
					? (uint8_t)(toupper(c))
					: (uint8_t)        (c);

				if( allowed && strchr(allowed,CONVCHAR) == NULL ) {
					fprintf( stderr,
						"Non DNA character ('%c') at file offset %ld sequence in %d.\n",
						c, nbytes, seq_count+1 );
				}

				// Add it to hash buffer, flushing it if it is full.
				hashbuff[ buflen++ ] = CONVCHAR;
				if( buflen >= SIZEOF_HASHBUFF ) {
					MD5_Update( &ctx, hashbuff, SIZEOF_HASHBUFF );
					buflen = 0;
				}
				seqlen++;

			}
		}
	}

	if( buflen > 0 ) {
		MD5_Update( &ctx, hashbuff, buflen );
		buflen = 0;
	}

	if( seqlen > 0 ) {
		MD5_Final( digest, &ctx );
		_emit_length_sep_digest( digest, MD5_DIGEST_LENGTH, seqlen, fp_out );
		seqlen = 0;
		seq_count += 1;
	}

	return seq_count;
}


static const char *USAGE =
"%s [ options ] < input.fa > output.fa.md5\n"
"--allow | -a   verify sequence chars are among specified character set [%s]\n"
"--sep   | -s   character separating FASTA headers from length and digest in output [0x%02x]\n"
"--icase | -c   generate case-INsensitive hashes (ie. ignore soft masking)[%s]\n"
" --help | -?   show this message\n"
"Notes:\n"
"1. Every character that is NOT a) in a FASTA header or b) a whitespace\n"
"   character as defined by isspace is included in sequence hash(es).\n"
"2. By default any character is allowed in sequences (they are unchecked),\n"
"   and sequences are hashed exactly as they are (case-sensitively).\n"
"3. The --icase option causes all sequence characters to be converted to\n"
"   upper case before being hashed (or verified!), so...\n"
"4. if --icase is given, only upper-case characters need be (or should be!)\n"
"   included in --allow.\n"
#ifdef _DEBUG
"This is a debug build.\n"
#endif
"\n";

static void _print_usage( const char *exename, FILE *fp ) {
	fprintf( fp, USAGE,
		exename,
		_allowed,
		_separator,
		_ignore_case ? "yes" : "no" );
}

int main( int argc, char *argv[] ) {

	do {
		static const char *CHAR_OPTIONS = "a:s:c?";
		static struct option LONG_OPTIONS[] = {
			{"allow", 1,0,'a'},
			{"sep",   1,0,'s'},
			{"icase", 0,0,'c'},
			{ NULL,   0,0, 0 }
		};
		int opt_index;
		const int c = getopt_long( argc, argv, CHAR_OPTIONS, LONG_OPTIONS, &opt_index );
		if( -1 == c ) break;
		switch( c ) {
		case 'a':
			_allowed = optarg;
			break;
		case 's':
			_separator = optarg[0];
			break;
		case 'c':
			_ignore_case = true;
			break;
		default:
			_print_usage( argv[0], stdout );
			exit(0);
		}
	} while(true);

	return _process( stdin,
		strlen(_allowed) > 0 ? _allowed : NULL,
		_ignore_case,
		stdout ) > 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}

