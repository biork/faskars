
#ifndef _ramindex_h_
#define _ramindex_h_

struct ramindex; // opaque

struct contig {
	const char *name;
	unsigned long offset;
	unsigned long length;
	unsigned int line_chars;
	unsigned int line_bytes;
};

int load_index( const char *index_filename, struct ramindex **ppidx );
const struct contig *find( const char *contig_name, const struct ramindex *pidx );
void free_index( struct ramindex *pidx );

#endif

