
#include <stdlib.h>
#include <ctype.h>
#include "region.h"

#define IS_NUL(c) (c=='\0')
#define NOT_NUL(c) (c!='\0')

/**
 * Parse a 
 * 1. contig name,
 * 2. start coordinate and 
 * 3. end coordinate
 * ...in that order from a string in the most lenient way possible.
 *
 * The string should satisfy this regular expression:
 *
 *   /\s*[a-zA-Z0-9_.]+.[^0-9]*[0-9]+.[^0-9]*[0-9]+/
 */
const char * parse_region( char *buffer, long *pstart, long *pend ) {

	const char *contig = NULL;
	char *sp, *pc = buffer;

	// Find start of CONTIG NAME.
	while( *pc && isspace(*pc) ) pc++;
	if( IS_NUL(*pc) )
		return NULL;
	contig = pc++;
	// Find end of CONTIG NAME.
	while( *pc && ( isalnum(*pc) || *pc == '_' || *pc == '.' ) ) pc++;
	if( IS_NUL(*pc) )
		return NULL;
	*pc++ = '\0';

	// Find start of START COORDINATE.
	while( *pc && ! isdigit(*pc) ) pc++;
	if( IS_NUL(*pc) )
		return NULL;
	sp = pc++;
	// Find end of START COORDINATE.
	while( *pc && isdigit(*pc) ) pc++;
	if( IS_NUL(*pc) )
		return NULL;
	*pc++ = '\0';
	*pstart = strtol( sp, NULL, 10 );

	// Find start of END COORDINATE.
	while( *pc && ! isdigit(*pc) ) pc++;
	if( IS_NUL(*pc) )
		return NULL;
	sp = pc++;
	// Find end of END COORDINATE.
	while( *pc && isdigit(*pc) ) pc++;
	*pc++ = '\0';
	*pend = strtol( sp, NULL, 10 );

	return contig;
}


#ifdef _UNIT_TEST_REGION_
#include <stdio.h>
#include <string.h>
static const char *CASES[] = {
	"f_5\t1\t2",
	" f_5\t33\t43 ",
	"5__a:555 6789 ",
	"5__a:555 6789 ",
	"chr1,12-13",
	"chrX 12 13",
	"1 111222333 222333444",
	"Y 111222333 222333444",
	"chr8 junk111222333,garbage222333444"
};
int main( int argc, char *argv[] ) {
	for(int i = 0; i < sizeof(CASES)/sizeof(char*); i++ ) {
		char request[ 64 ];
		strcpy( request, CASES[i] );
		long start = 0;
		long end = 0;
		const char *contig = parse_region( request, &start, &end );
		if( contig ) {
			printf( "\"%s\"\t%s\t%ld\t%ld\n", CASES[i], contig, start, end );
		} else {
			printf( "\"%s\"\tfailed\n", CASES[i] );
		}
	}
	return EXIT_SUCCESS;
}
#endif

