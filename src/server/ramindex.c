
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <assert.h>

#include "ramindex.h"

#define IS_NUL(c) (c=='\0')
#define NOT_NUL(c) (c!='\0')

/**
 * Works much like strtok_r where delim is any whitespace.
 * Assumes a corpus:
 * 1. of NL-delimited LINES containing
 * 2. whitespace-free FIELDS delimited by ARBITRARY WHITESPACE
 * 3. that is NUL terminated at VERY END.
 */
static char *stoken(char *str, char **saveptr) {

	char *token = NULL;
	if( str == NULL ) {
		str = *saveptr;
	}

	// Skip initial whitespace
	while( NOT_NUL(*str) && isspace(*str) ) str++;

	if( NOT_NUL(*str) ) {
		token =  str;
	}

	// Find next run of whitespace
	while( NOT_NUL(*str) && ! isspace(*str) ) str++;

	if( isspace(*str) ) {
		*str++ = '\0';
	}

	*saveptr = str;

	return token;
}


struct ramindex {
	int count;
	struct contig *content;
	char raw[0]; // to allow for final NUL being added by strtok
};

static int compare_contigs( const void *pvl, const void *pvr ) {
	const struct contig *pl = (const struct contig *)pvl;
	const struct contig *pr = (const struct contig *)pvr;
	return strcmp( pl->name, pr->name );
}

/**
 * Make an *.fai file memory-resident and sorted for fast _repeated_
 * lookups (by binary search).
 * NW_004070883.1	389631	3221608944	80	81
 * NW_004070884.1	390496	3222003547	80	81
 */
int load_index( const char *index_filename, struct ramindex **ppidx ) {

	int count;
	struct stat info;
	if( stat( index_filename, &info ) != 0 ) {
		perror( "stat'ing genome index file" );
		return -1;
	}

	*ppidx = NULL;

	struct ramindex *pidx = calloc( 1, sizeof(struct ramindex)+info.st_size+1 );

	if( pidx ) {
		FILE *fp = fopen( index_filename, "r" );
		if( fp ) {

			// Load the index as one big string.

			if( fread( &pidx->raw, sizeof(char), info.st_size, fp ) != info.st_size ) {
				perror( "reading genome index file" );
				free( pidx );
				return -1;
			}
			assert( IS_NUL( pidx->raw[info.st_size] ) ); // Insure NUL-termination.
			fclose( fp );
		} else {
			perror( "opening genome index file" );
			free( pidx );
			return -1;
		}

		// Count lines by counting line terminators to determine
		// how many entries we need in the index.

		count = 1; // ...in case final line is not NL-terminated.

		for(int i = 0; i < info.st_size; i++ ) {
			if( pidx->raw[i] == '\n' ) {
				count += 1;
			}
		}

		// Parse index file filling in index entries.

		pidx->content = calloc( sizeof(struct contig), count /* might be 1 extra */ );

		if( pidx->content ) {
			char *saved = NULL;
			unsigned int field = 0;
			const char *token = stoken( pidx->raw, &saved );
			struct contig *pcc = pidx->content;
			while( token ) {
				switch( field ) {
				case 0: // name
					pcc->name = token;
					break;
				case 1: // length
					pcc->length = strtol( token, NULL, 10 );
					break;
				case 2: // offset
					pcc->offset = strtol( token, NULL, 10 );
					break;
				case 3: // line characters
					pcc->line_chars = atoi( token );
					break;
				case 4: // line bytes
					pcc->line_bytes = atoi( token );
				// No default because field > 4 is provably impossible.
				// Nonsensical index content, however, is possible! See below.
				}
				if( field < 4 ) {
					field += 1;
				} else {
					if( strlen(pcc->name) < 1
							|| pcc->length < 1
							|| pcc->offset < 3 // minimum in valid FASTA file
							|| pcc->line_chars < 1
							|| pcc->line_bytes <= pcc->line_chars ) {
						fprintf( stderr, "bad index line: %d\n", pidx->count+1 );
						free( pidx->content );
						pidx->content = NULL;
						break;
					} else {
						field = 0;
						pcc++;
						pidx->count += 1;
					}
				}
				token = stoken( NULL, &saved );
			}

		} else {
			perror("allocating array for index entries");
		}

	} else {
		perror("allocating buffer for index");
		return -1;
	}

	// Don't like returning from loops, so...
	
	if( pidx->content == NULL ) {
		free( pidx );
		return -1;
	}

#ifdef DEBUG
#ifdef ECHO_INDEX
	// For testing: the following output should match verbatim the input
	// (as long as the input is tab-delimited without extra whitespace).
	for(int i = 0; i < pidx->count; i++ ) {
		struct contig *pcontig = pidx->content + i;
		fprintf( stdout, "%s\t%ld\t%ld\t%d\t%d\n",
				pcontig->name,
				pcontig->length,
				pcontig->offset,
				pcontig->line_chars,
				pcontig->line_bytes );
	}
#endif
#endif

	qsort( pidx->content, pidx->count, sizeof(struct contig), compare_contigs );

	*ppidx = pidx;

	return 0;
}


/**
 */
const struct contig *find( const char *contig_name, const struct ramindex *pidx ) {
	// bsearch expects its 'key' argument to be the same type as the array
	// contents, so we can't just pass in the contig_name!
	struct contig dummy = {
		.name = contig_name,
	};
	return bsearch( &dummy, pidx->content, pidx->count, sizeof(struct contig), compare_contigs );
}


void free_index( struct ramindex *pidx ) {
	if( pidx ) {
		if( pidx->content ) {
			free( pidx->content );
		}
		free( pidx );
	}
}


#ifdef _UNIT_TEST_RAMINDEX_

/**
 * If no second argument (contig name) is present and ECHO_INDEX is
 * defined, this executable will just echo the index to stdout. It
 * can then be compared to the input to validate the parse.
 *
 * Validating correct lookup requires trying every contig in the
 * index as the 2nd command line argument and confirming it is
 * returned.
 */
int main( int argc, char *argv[] ) {

	int exit_status = EXIT_FAILURE;
	struct ramindex *pidx = NULL;

	if( load_index( argv[1], &pidx ) == 0 ) {
		if( argc > 2 ) {
			const struct contig *pcontig = find( argv[2], pidx );
			if( pcontig ) {
				printf( "%s\t%ld\t%ld\t%d\t%d\n",
					pcontig->name,
					pcontig->offset,
					pcontig->length,
					pcontig->line_chars,
					pcontig->line_bytes );
				exit_status = EXIT_SUCCESS;
			} else {
				printf( "'%s' not found\n", argv[2] );
			}
		}
		free_index( pidx );
	}
	return exit_status;
}

#endif

