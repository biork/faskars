
from sys import stderr
from os import stat,mkfifo,waitpid,unlink
from os.path import splitext,join as path_join,basename,exists
from subprocess import Popen
from tempfile import gettempdir
from os import getenv

class FastaServer:
	"""
	A Python interface to the FIFO-based serve-fasta executable.
	"""

	# From #include <bits/stat.h>...
	S_IFIFO = 0o0010000

	ARGS = ('fastaname','fifoname')

	def __init__( self, *files ):
		"""
		At least one of filename and fifoname must be provided; optionally both
		may be.

		The (binary) server MUST be launched with a pre-existing FIFO, so...
		1. Provision of filename implies self is responsible for launching
			the server on either an existing FIFO or a newly created one.
		2. If only the fifoname is provided it must exist since self cannot
			launch the server and the fifo must have existed before server launch.
		"""
		if len(files) < 1:
			raise RuntimeError( 
				"at least one of FASTA filename or fifoname must be specified" )
		if any( not exists(name) for name in files ):
			raise RuntimeError(
				"one or more of provided filenames does not exist" )
		# Determine what type of file first argument is and assign it to the
		# appropriate member attribute.
		if (stat(files[0]).st_mode & FastaServer.S_IFIFO) != 0:
			self.fifoname = files[0]
		else:
			self.fastaname = files[0]

		# Similarly assign the second file, if provided and
		# verify it's not redundant!

		if len(files) >= 2:
			if stat(files[1]).st_mode & FastaServer.S_IFIFO != 0:
				if hasattr(self,"fifoname"):
					raise RuntimeError("two FIFOs specified")
				self.fifoname = files[1]
			else:
				if hasattr(self,"fastaname"):
					raise RuntimeError("two FASTAs specified")
				self.fastaname = files[1]

		if __debug__:
			print( "FIFO:{}, FASTA:{}".format( self.fifoname, self.fastaname ), file=stderr )

		self.owns_fifo = False
		# If no fifo name was provided then this class is responsible for 
		# everything: creating the FIFO AND exec'ing the server.
		if not hasattr(self,"fifoname"):
			self.fifoname = path_join( gettempdir(), splitext( basename(self.fastaname) )[0] + '.fifo' )
			if not exists( self.fifoname ):
				mkfifo( self.fifoname )
				self.owns_fifo = True
		# ...however, only exec the server if a fastaname was provided. 
		# Otherwise, we assume something else has already run the server.
		if hasattr(self,"fastaname"):
			# TODO: Assuming a full path was provided in SERVER environment
			# variable or else it's on the PATH...
			self.child = Popen( [ getenv('SERVER','serve-fasta'), self.fifoname, self.fastaname ] )


	def __del__( self ):
		try:
			if any( hasattr(self,a) for a in FastaServer.ARGS ):
				self.shutdown()
		except NameError as x:
			print( x, "(This is an acknowledged bug in some Python3 releases.)" )


	def shutdown( self ):
		"""
		An acknowledge bug in some Python releases...
		(https://stackoverflow.com/questions/64679139/nameerror-name-open-is-not-defined-when-trying-to-log-to-files)
		...makes explicit shutdown preferable to implicit (in __del__).
		Thus, this method.
		"""
		if hasattr(self,"child"):
			# Trigger a controlled exit then remove the.
			with open( self.fifoname, 'w' ) as fp:
				fp.write( '\n' )
			waitpid( self.child.pid, 0 )
		if self.owns_fifo:
			unlink( self.fifoname )
		for a in FastaServer.ARGS:
			if hasattr(self,a):
				delattr(self,a)


	def get_sequence( self, contig, start, end ) -> str:
		"""
		"""
		# Write the sought identifiers to the fifo...
		with open( self.fifoname, 'w', encoding="ASCII" ) as fp:
			fp.write( "{}:{}-{}\n".format( contig, start, end ) )
		# ...and read back the results.
		with open( self.fifoname, 'r', encoding="ASCII" ) as fp:
			results = fp.readline().rstrip() # tuple( line.rstrip() for line in fp.readlines() )
			return results


if __name__=="__main__":
	from sys import argv,exit
	import gc
	if len(argv) < 2:
		print( argv[0], "<contig> <start> <end> [ <FASTA filename> ] [ <fifoname> ]" )
		print( "Optionally, define SERVER=<path to fasta-server> environment variable." )
		exit(-1)
	server = FastaServer( *argv[4:] )
	print( server.get_sequence( argv[1], argv[2], argv[3] ) )
	print( server.get_sequence( argv[1], argv[2], argv[3] ) )
	server.shutdown()

