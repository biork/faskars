
"""
Use fasta-server and a "SAF" file (that is input for Subread from
https://bioinf.wehi.edu.au/software/index.html) to quantify GC content in
genes and exons.

SAF files are 6-column tab-separated-value files with a single line header.
Columns are: GeneID, Chr, Start, End, Strand, Part and the Start and End
columns are 1-based and inclusive.

The SAF file input to this script is expected to contain _exonic_
annotations grouped by the parent genes, so we add up gc content from
lines with same gene identifier.
"""

from sys import argv,stdin,stdout
from os.path import exists
from server import FastaServer
import argparse

def _gc( seq ) -> int:
	n = 0
	for c in seq:
		if c in "cCgG":
			n += 1
	return n


def _emit( tag, label, gc, n ):
	print( tag, label, gc, n, "{:0.6f}".format(gc/n), sep="\t" )
	if __debug__:
		stdout.flush()

def _main( fp, fifo, fasta ):

	server = FastaServer( fifo, fasta )

	# Expect a single-line header
	assert fp.readline().startswith("GeneID"),"It's a subread \"SAF\" file"
	nbp = 0
	gct = 0
	cur = ""
	for line in fp:
		geneid,contig,start,end,strand,part = line.rstrip().split('\t')
		if geneid != cur:
			if gct > 0:
				_emit( "GE", cur, gct, nbp )
			nbp = 0
			gct = 0
			cur = geneid
		s = server.get_sequence( "{}{}".format( args.prefix, contig ), start, end )
		if len(s) == 0:
			print( "Failed", line, file=stderr )
		gc = _gc( s )
		_emit( "EX", "{}/{}".format( geneid, part ), gc, len(s) )
		gct += gc
		nbp += len(s) 
	else:
		_emit( "GE", cur, gct, nbp )

############################################################################

parser = argparse.ArgumentParser(
	description="""\
Use fasta-server to fetch genomic intervals to count G/C bases.
""",
	formatter_class=argparse.RawDescriptionHelpFormatter,
	epilog="""\
A BED file containing regions to be quantified should be sent to stdin.
""" )

parser.add_argument( "--fasta", required=True, metavar="FILENAME",
	help="The genome (multi-sequence) FASTA file." )

parser.add_argument( "--fifo", required=True, metavar="FILENAME",
	help="The name of a FIFO file, possibly pre-created to use to communicate with the server." )

parser.add_argument( "--prefix", default="", metavar="FILENAME",
	help="Prefix (e.g. \"chr\") to add to otherwise bare chromosome identifiers.[\"%(default)s\"]" )

args = parser.parse_args()

if exists(args.fasta) and exists(args.fifo):
	_main( stdin, args.fasta, args.fifo )

