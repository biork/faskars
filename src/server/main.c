
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/mman.h>
#include <getopt.h>
#include <assert.h>

#include "region.h"
#include "ramindex.h"

#define MAXLEN_REQUEST (127)

/**
 * Stream the sequence AND ONLY THE SEQUENCE out the FILE pointer.
 * In particular, skipping newlines and any other whitespace encountered
 * (though there should not be any). If we encounter another sequence
 * header before reaching <end> simply stop; caller should know what
 * they're doing with respect to length.
 */
static unsigned long stream( const struct contig *pcontig,
		const unsigned long start, // 1-based fully closed!
		      unsigned long end,   // 1-based fully closed!
		const char *seq, FILE *fp ) {

	unsigned long N, n = 0; // N is initialized below!

	// First get to the relevant contig...
	const char *pc = seq + pcontig->offset;
	// ...then the beginning of the required line...
	const long nlines   = (start-1) / pcontig->line_chars;
	pc += (nlines*pcontig->line_bytes);
	// ...and finally the offset into the line.
	const long line_off = (start-1) % pcontig->line_chars;
	pc += line_off;

	// Clamp end to the length given in the index which, assuming the
	// index is correct, obviates any further conditionals in the loop!

	end = end > pcontig->length ? pcontig->length : end;

	assert( start <= end );

	N = end-start+1;

	do {
		int c = *pc++;
		if( ! isspace(c) ) {
			fputc( c, fp );
			n += 1;
#if defined(LINE_WIDTH)
			if( n % LINE_WIDTH == 0 ) {
				fputc('\n', fp );
			}
#endif
		}
	} while( n < N );

#if defined(LINE_WIDTH)
	if( n % LINE_WIDTH ) { // ...to avoid redundant NL at end.
		fputc('\n', fp );
	}
#endif

	return n;
}


static int serve( const char *genome, struct ramindex *index, const char *fifoname ) {

	const char *BAD_REQ = "!bad_request\n";	
	char request[ MAXLEN_REQUEST+1 ];
	long start, end;
	FILE *fp = NULL;

	do {

		fp = fopen( fifoname, "r" );
		if( fp ) {
			int nrd = 0;
			while( ! feof( fp ) ) {
				int n = fread( request + nrd, sizeof(char), MAXLEN_REQUEST - nrd, fp );
				if( n >= 0 ) {
					nrd += n;
				}
			}
			fclose( fp );
		} else {
			perror("opening fifo");
			break;
		}

		// An empty line triggers (controlled) exit.
		
		if( request[0] == '\n' ) {
			break;
		}

		// We have to send something, even if it's an empty line since
		// client expects _something_...and will be _blocking_ for it.
		fp = fopen( fifoname, "w" );
		if( fp ) {
			const char *contig_name = parse_region( request, &start, &end );
			if( contig_name ) {
				const struct contig *pcontig = find( contig_name, index );
				if( pcontig && start <= end ) {
						stream( pcontig, start, end, genome, fp );
				} else {
					fputs( BAD_REQ, fp );
				}
			} else {
				fputs( BAD_REQ, fp );
			}
			fclose( fp );
		}

	} while( 1 );

	return 0;
}

static long  genome_len  = -1;
static int   genome_fd   = -1;
static char *genome_data = NULL;

int map_genome( const char *sequence_filename ) {

	struct stat info;
	if( stat( sequence_filename, &info ) ) {
		perror( "stat'ing genome sequence file" );
		return -1;
	}

	genome_len = info.st_size;
	genome_fd  = open( sequence_filename, 0 );

	if( ! ( genome_fd < 0 ) ) {
		genome_data = mmap(NULL, // start anywhere (in userspace)
					genome_len,   // dictated by driver
					PROT_READ,    // required
					MAP_SHARED,   // recommended
					genome_fd, 
					0 );          // a "handle" provided by driver

		if( MAP_FAILED == genome_data ) {
			perror( "mmap'ing genome sequence" );
			close( genome_fd );
			return -1;
		}
	}
	return 0;
}


static const char *USAGE = 
"%s <fifoname> <genome file> [ <genome index> ] \n"
"Options:\n"
"Written by roger.kramer@uef.fi. (Compiled on %s %s) \n";


static void print_usage( const char *exename, FILE *fp ) {
	fprintf( fp, USAGE, exename,
		__DATE__, __TIME__ );
}


int main( int argc, char * argv[] ) {

	static char index_filename[ FILENAME_MAX+1 ];
	struct ramindex *index = NULL;

	if( argc < 3 ) {
		print_usage( argv[0], stdout );
		exit(0);
	}

	// Index filename is made from genome filename unless it is 
	// explicitly provided.

	if( argc > 3 ) {
		strncpy( index_filename, argv[3], FILENAME_MAX );
	} else {
		strncpy( index_filename, argv[2], FILENAME_MAX );
		strcat( index_filename, ".fai" );
	}

	// If we can't load the index we're dead in the water; do it first.

	if( load_index( index_filename, &index ) != 0 ) {
		exit(-1);
	}

	if( map_genome( argv[2] ) == 0 ) {
		serve( genome_data, index, argv[1] );
		munmap( genome_data, genome_len );
		close( genome_fd );
	}

	if( index ) {
		free_index( index );
	}

	return EXIT_SUCCESS;
}

