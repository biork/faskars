
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <getopt.h>
#include <assert.h>
#include <errno.h>

#define NUL ('\0')
#define OUTPUT_LINE_LEN (70)
#define SIZEOF_HISTOGRAM (128)

static const char *MSG_NON_ASCII = "non-ASCII character %s file at offset %ld\n";
static const char *MSG_UNEXPECTED_HEADER = "unexpected header prefix in %s file at offset %ld\n";
static const char *MSG_NON_PRINTABLE_CHAR = "unprintable char (0x%02x) in %s file at offset %ld\n";
static const char *L_FILE = "1st";
static const char *R_FILE = "2nd";


static void _flush_histograms( int *lhist, int *rhist ) {
	for(int i = 21 /* begin after space */; i < SIZEOF_HISTOGRAM; i++ ) {
		if( lhist[i] > 0 || rhist[i] > 0 ) {
			fprintf( stdout, "# %02X\t%c\t%d\t%d\n", i, i, lhist[i], rhist[i] );
		}
	}
}


static int _skip_fasta_header( FILE *fp ) {
	int c = getc( fp );
	while( c != '>' && ! isalpha(c) ) { c = getc( fp ); }
	if( c == '>' ) {
		do { c = getc( fp ); } while( c != '\r' && c != '\n' ); 
		do { c = getc( fp ); } while( ! isalpha(c) ); 
	}
	return c;
}


static int _process( FILE *L_fp, FILE *R_fp, bool ignore_case, FILE *fp_out ) {

	bool in_difference = false;
	int seg_count = 0;
	int *lhist = calloc(SIZEOF_HISTOGRAM*2,sizeof(int));
	int *rhist = lhist + SIZEOF_HISTOGRAM;

	char buffer[ OUTPUT_LINE_LEN+1 ];
	int buffered = 0;

	if( lhist == NULL ) {
		return ENOMEM;
	}

	// Skip headers 

	int Lc = _skip_fasta_header( L_fp );
	int Rc = _skip_fasta_header( R_fp );

	while( Lc != EOF && Rc != EOF ) {

		// Quietly skip spaces in either sequence.

		if( isspace(Lc) ) {
			Lc = getc(L_fp);
			continue;
		}
		if( isspace(Rc) ) {
			Rc = getc(R_fp);
			continue;
		}

		// We expect ASCII. Verify it!

		if( Lc > 127 ) {
			fprintf( stderr, MSG_NON_ASCII, L_FILE, ftell(L_fp)-1 );
			seg_count = -1;
			break;
		}

		if( Rc > 127 ) {
			fprintf( stderr, MSG_NON_ASCII, R_FILE, ftell(R_fp)-1 );
			seg_count = -1;
			break;
		}

		// We're expressly expected SINGLE-SEQUENCE FASTA files. Verify it!

		if( Lc == '>' ) {
			fprintf( stderr, MSG_UNEXPECTED_HEADER, L_FILE, ftell(L_fp)-1 );
			seg_count = -1;
			break;
		}
		if( Rc == '>' ) {
			fprintf( stderr, MSG_UNEXPECTED_HEADER, R_FILE, ftell(R_fp)-1 );
			seg_count = -1;
			break;
		}

		if( ! isprint(Lc) ) {
			fprintf( stderr, MSG_NON_PRINTABLE_CHAR, Lc, L_FILE, ftell(L_fp)-1 );
			seg_count = -1;
			break;
		}
		if( ! isprint(Rc) ) {
			fprintf( stderr, MSG_NON_PRINTABLE_CHAR, Rc, R_FILE, ftell(R_fp)-1 );
			seg_count = -1;
			break;
		}

		if( ignore_case ) {
			Lc = toupper(Lc);
			Rc = toupper(Rc);
		}

		if( in_difference ) {

			if( Lc == Rc ) {

				buffer[ buffered ] = NUL;
				fprintf( fp_out, "\n>%s\n", buffer );
				buffered = 0;

				_flush_histograms( lhist, rhist );
				memset( lhist, 0, SIZEOF_HISTOGRAM*2 );

				in_difference = false;

			} else {

				// Update the histograms.

				lhist[ Lc ] += 1;
				rhist[ Rc ] += 1;

				// Output 1st file's char and buffer the second...

				if( buffered > 0 ) {
					fputc( Lc, fp_out );
				} else {
					fprintf( fp_out, "<%c", Lc );
				}
				buffer[ buffered++ ] = Rc;

				// ...and flush the buffer if and only if we have finished a line.
				
				if( buffered == OUTPUT_LINE_LEN ) {
					buffer[ buffered ] = NUL;
					fprintf( fp_out, "\n>%s\n", buffer );
					buffered = 0;
				}
			}

		} else {

			if( Lc != Rc ) {

				fprintf( fp_out, "<%c", Lc );
				buffer[ buffered++ ] = Rc;

				in_difference = true;
				seg_count += 1;
			}
		}

		Lc = getc(L_fp);
		Rc = getc(R_fp);

	} // while

	// If file's end in difference (AND we didn't arrive here because of
	// error) don't forget to do final histogram flush.
	
	if( seg_count >= 0 && in_difference ) {
		buffer[ buffered ] = NUL;
		fprintf( fp_out, "\n>%s\n", buffer );
		_flush_histograms( lhist, rhist );
	}

	if( lhist ) {
		free( lhist );
	}

	return seg_count;
}


static const char *USAGE =
"%s [ options ] left.fa right.fa > diffs.txt\n"
"--icase | -c   generate case-INsensitive hashes (ie. ignore soft masking)[%s]\n"
" --help | -?   show this message\n"
"Notes:\n"
"1. Every character that is NOT a) in a FASTA header or b) a whitespace\n"
"   character as defined by isspace is compared.\n"
"2. The --icase option causes all sequence characters to be converted to\n"
"   upper case before being hashed (or verified!).\n"
#ifdef _DEBUG
"This is a debug build.\n"
#endif
"\n";

static bool _ignore_case = false;

static void _print_usage( const char *exename, FILE *fp ) {
	fprintf( fp, USAGE,
		exename,
		_ignore_case ? "yes" : "no" );
}

int main( int argc, char *argv[] ) {

	int segment_count = -1; // "failure" unless overruled by _process.

	do {
		static const char *CHAR_OPTIONS = "c?";
		static struct option LONG_OPTIONS[] = {
			{"icase", 0,0,'c'},
			{ NULL,   0,0, 0 }
		};
		int opt_index;
		const int c = getopt_long( argc, argv, CHAR_OPTIONS, LONG_OPTIONS, &opt_index );
		if( -1 == c ) break;
		switch( c ) {
		case 'c':
			_ignore_case = true;
			break;
		default:
			_print_usage( argv[0], stdout );
			exit(0);
		}
	} while(true);

	// Try opening both input files.

	FILE *l_fp = fopen( argv[optind+0], "r" );
	FILE *r_fp = fopen( argv[optind+1], "r" );

	// ...and process if and only if both are open.

	if( l_fp != NULL && r_fp != NULL ) {
		segment_count = _process( l_fp, r_fp, _ignore_case, stdout );
	}

	// Close anything open. Anything NOT open was a failure; report it!

	if( r_fp ) {
		fclose( r_fp );
	} else {
		perror( "opening 1st file" );
	}
	if( l_fp ) {
		fclose( l_fp );
	} else {
		perror( "opening 2nd file" );
	}

	return segment_count >= 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}

