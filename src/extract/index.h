
#ifndef _index_h_
#define _index_h_

#define MAXLEN_CONTIG_NAME (63)

struct _indexrec {
	char					contig[ MAXLEN_CONTIG_NAME+1 ];
	unsigned int	seqlen;
	off_t					byte_offset;
	unsigned int	linechars;
	unsigned int	linebytes;
};

int lookup( const char *genome, const char *contig, int quiet, struct _indexrec *rec );

#endif

