
#include <string.h>
#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <fcntl.h>
//#include <unistd.h>
//#include <sys/mman.h>

#include "index.h"

int _lookupfp( FILE *fp, const char *contig, int quiet, struct _indexrec *rec ) {

#define N_EXPECTED_COLUMNS (5)

	int err = -1; 
	char *buf = NULL;
	int linenum = 1;

	assert( contig != NULL );
	assert( strlen(contig) > 0 );
	assert( fp  != NULL );
	assert( rec != NULL );

	while( ! feof(fp) ) {

		if( buf != NULL ) {
			free( buf );
			buf = NULL;
		}

		// Parse a line

		if( fscanf( fp, "%ms\t%d\t%ld\t%d\t%d\n",
			&buf,
			&rec->seqlen,
			&rec->byte_offset,
			&rec->linechars,
			&rec->linebytes ) != N_EXPECTED_COLUMNS ) {
			if( ! quiet ) {
				fprintf( stderr, "error: error parsing line %d of index and/or "
						"it did not contain the expected %d columns\n",
					linenum, N_EXPECTED_COLUMNS );
			}
			break;
		}

		// Does it contain the contig name we seek?

		if( buf != NULL && strcmp( buf, contig ) == 0 ) {

			// Yes, we found the relevant record.

			if( strlen( buf ) <= MAXLEN_CONTIG_NAME /* room to store it? */ ) {

				strncpy( rec->contig, buf, MAXLEN_CONTIG_NAME );
				err = 0; // This is the only successful exit path.
				break;

			} else
			if( ! quiet ) {
				fprintf( stderr, "error: contig name %s in index is %ld chars, "
						"maximum contig name length is %d\n",
					buf, strlen(buf), MAXLEN_CONTIG_NAME );
				break;
			}
		}

		// ...keep searching.

		linenum += 1;
	}

	if( buf != NULL )
		free( buf );

	return err;
}


/**
 * Lookup a contig by scanning the index file.
 */
int lookup( const char *fasta_filename, const char *contig, int quiet, struct _indexrec *rec ) {

	int err = -1;
	static char index[ FILENAME_MAX+1 ];

	assert( fasta_filename != NULL );
	assert( strlen(fasta_filename) > 0 );

	if( snprintf( index, FILENAME_MAX, "%s.fai", fasta_filename ) < FILENAME_MAX ) {
		FILE *fp = fopen( index, "r" );
#ifdef _DEBUG
		fprintf( stderr, "using index: %s\n", index );
#endif
		if( fp ) {
			err = _lookupfp( fp, contig, quiet, rec );
			fclose( fp );
		} else
		if( ! quiet ) {
			fprintf( stderr, "error: index file (%s) is missing or unreadable\n",
				index );
		}
	} else
	if( ! quiet ) {
		fprintf( stderr, "error: genome path (%s) is too long (with .fai suffix) for buffer (%d)\n",
			fasta_filename, FILENAME_MAX );
	}

	return err;
}


#ifdef _UNIT_TEST_INDEX_
int main( int argc, char *argv[] ) {

	struct _indexrec info;
	memset( &info, 0, sizeof(info) );

	if( argc > 2 ) {
		if( lookup( argv[1], argv[2], 0 /* not quiet */, &info ) == 0 ) {
			printf( "%s\t%d\t%ld\t%d\t%d\n",
				info.contig,
				info.seqlen,
				info.byte_offset,
				info.linechars,
				info.linebytes );
			return EXIT_SUCCESS;
		}
	}

	return EXIT_FAILURE;
}

#endif

//# To exhaustively test an index...
//cut -f 1 genome.fa.fai | while read C; do ./test-index genome.fa ${C}; done > /tmp/foo
//md5sum genome.fa.fai /tmp/foo
