
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h> // for pipe and getopt
#include <errno.h>  // for perror
#include <stdbool.h>
#include <getopt.h>

/**
  * This assumes that the next line is the start of relevant sequence.
  * Returns a negative number on error, otherwise the number of 
  * alphabetic characters [a-zA-Z] actually copied.
  */
extern int excise( FILE *fp, int start, int end, int charlen, int bytelen, char *obuf, size_t buflen );

#include "index.h"

#ifndef NL_LEN
#error Makefile must define NL_LEN for the platform.
#endif

#define LEN_LINE_BUF (1024)
#define DEFAULT_END (1048576)

#ifndef __min
#define __min(x,y) ((x)<(y)?(x):(y))
#endif
#ifndef __max
#define __max(x,y) ((x)>(y)?(x):(y))
#endif

static char _header[ LEN_LINE_BUF+1 ];
static char _contig[ LEN_LINE_BUF+1 ];

static int  _linelen    = 0;
static bool _complement = false;
static int  _o_width    = 70;
static bool _fewer_ok   = false;
static bool _append     = false;
static int  _verbosity  = 0;

static int (*_emit)(int, FILE *) = fputc;

/**
	* Linearly scan for a FASTA line merely containing the sought string.
	* TODO: This is far too permissive by default; should be a regexp.
	*/
static bool _find( FILE *fp, const char *contig ) {

	while( ! feof(fp) ) {
		if( fgets( _header, LEN_LINE_BUF, fp ) ) {
			if( _header[0] == '>' && strstr( _header, contig ) )
				return true;
		}
	}
	return false;
}


/**
  * This is only valid for uppercase AGCT. The 2nd and 3rd least sig
  * bits of the ASCII for these four chars cover 0-3, so by shifting
  * them into the LSB position and masking, one obtains a perfect hash
  * and no conditionals are required.
  * A 0x41  a 0x61   0001
  * C 0x43  c 0x63   0011
  * G 0x47  g 0x67   0111
  * T 0x54  t 0x74   0100
  * N 0x4e  n 0x6e   1110
  */
static int _fput_complement( int c, FILE *fp ) {
#ifdef HAVE_FAST_COMPLEMENT
	// Following conditional forces the output to respect the input case.
	if( islower(c) )
		c = ((c & 0xF) == 0xE ) ? 'n' : "tgac"[ (c>>1) & 0x3 ];
	else
		c = ((c & 0xF) == 0xE ) ? 'N' : "TGAC"[ (c>>1) & 0x3 ];
	// ...This deals properly with c == 'N' || c == 'n'.
#else
	/**
	  * Obviously this isn't as fast as it ought to be.
	  */
	c = toupper(c);
	if( 'A' == c )
		c = 'T';
	else
	if( 'T' == c )
		c = 'A';
	else
	if( 'C' == c )
		c = 'G';
	else
	if( 'G' == c )
		c = 'C';
#endif
	fputc( c, fp );
	return c;
}


static void write( int start, int end ) {
	if( _verbosity > 0 ) {

		const char *pc = _header;
		if( *pc ) {
			while( *pc && *pc != '\n' && *pc != '\r' ) fputc( *pc++, ofp );
		} else {
			fprintf( ofp, ">%s", _contig );
		}

		fprintf( ofp, ";%c:%d-%d", _complement ? 'c' : 'i', start, end );

		if( suffix ) {
			fprintf( ofp, "/%s\n", suffix );
		} else {
			fputc( '\n', ofp );
		}
	}

	if( end < start ) {

		int e = 0;		
		int i = n;
		while( i-- > 0 ) {
			last_emission = _emit( obuf[i], ofp );
			if( ( ++e % _o_width ) == 0 ) 
				last_emission = fputc( '\n', ofp );
		}

	} else {

		int i = 0;
		while( i < n ) {
			last_emission = _emit( obuf[i++], ofp );
			if( ( i % _o_width ) == 0 ) 
				last_emission = fputc( '\n', ofp );
		}
	}

	/**
		* Only add a trailing newline if it wasn't the last character 
		* emitted.
		*/

	if( last_emission != '\n' )
		fputc( '\n', ofp );
}


static const char *USAGE = 
"%s [ options ] [ <fasta file> ] \n"
"Input options:\n"
"  -c <string> Find the contig with a header line containing <string>\n"
"              By default the first sequence encountered is used.\n"
"  -s <start>  Excise from <start> to...\n"
"  -e <end>    ...<end>. By default dump entire sequence (but see Note 4).\n"
"  -m <filename> A file containing a list of (start,end) intervals on the\n"
"                contig specified with the -c option.\n"
"  -l <len>    Non-terminal data lines in FASTA file have <len> characters,\n"
"              that is, <len> [ACGT] chars, excluding line termination.[%d]\n"
"Output options:\n"
"  -v 0 => just sequence [%d], \n"
"     1 => sequence with header.\n"
"          The output header is of the form:\n"
"          >input header;<strand>:<start>-stop[/<suffix>]\n"
"    >1 => debug\n"
"  -i          Complement the sequence [%s]\n"
"  -w <width>  Format output to <width> columns [%d]\n"
"  -S <suffix> Add the suffix <suffix> to the output header\n"
"              This is only relevant if verbosity > 0.\n"
"  -F          Allow fewer than |start-end|+1 bases in output.[%s]\n"
"              In other words, return a failure status if\n"
"              fewer than start-end+1 are excised.\n"
"  -X          Print requirements of FASTA format.\n"
"Notes\n"
"1. 'start' and 'end' are interpreted as 1-based, fully-closed intervals.\n"
"   In mathematical notation: [ start, end ].\n"
"2. Sequence is printed in the order specified by the start and end.\n"
"   parameters, so, yes, if end < start, the sequence is reversed.\n"
"   Complementation is NOT implicit though. Reverse-complementation\n"
"   requires BOTH end < start AND the -c option.\n"
"3. All FASTA file lines (including headers) must be no more than %d characters.\n"
"4. If 'end' is not specified the entire sequence UP TO %dbp will be printed.\n"
"5. A FASTA file index is automatically used if it is available.\n"
"   It should be the 5-column format created by \"samtools faidx\", and\n"
"   it should be named <fasta file>.fai (e.g. \"basename.fa.fai\").\n"
"6. Either:\n"
"   1. specifying the -l option, OR\n"
"   2. providing an index as generated by \"samtools faidx\"\n"
"   ...GREATLY increases lookup speed, but it is also entirely optional.\n"
"   We fall back to a (slow!) linear scan in the absence of an index or\n"
"   a line length hint.\n"
"7. This build assumes lines have Unix termination: a single \\n (0x0a)\n"
"   character unless an index overrides that!\n"
"Written by roger.kramer@uef.fi. (Compiled on %s %s) \n";

static const char *_string_rep( bool v ) {
	return v ? "true" : "false";
}


static void print_usage( const char *exename, FILE *fp ) {
	fprintf( fp, USAGE, exename,
		_linelen,
		_verbosity,
		_string_rep(_complement),
		_o_width,
		_string_rep( _fewer_ok ),
		LEN_LINE_BUF-1,
		DEFAULT_END,
		__DATE__, __TIME__ );
}


int main( int argc, char *argv[] ) {

	int n = 0;
	unsigned int start = 1;
	unsigned int end   = DEFAULT_END; // 2^20
	unsigned int lb, ub;
	int argi = 0;
	char last_emission = 0;
	char oname[ FILENAME_MAX+1 ];
	FILE *ofp = NULL;
	FILE *ifp = stdin;
	int exit_status = EXIT_FAILURE; // ...until otherwise!
	const char *suffix = NULL;
	char *obuf = NULL;
	size_t N = 0;
	const char *fasta_file_name = NULL;

	/**
	  * Required initializations.
	  */

	memset( oname, 0, sizeof(oname) );

	/**
	  * So that, in the absence of any other specification, this will seek 
	  * the first sequence (header) in the (possibly multi-sequence) FASTA 
	  * file...
	  */

	_contig[0] = '\0';
	_header[0] = '\0';

	do {
		static const char *CHAR_OPTIONS = "hc:s:e:v:id:w:l:a:o:S:F";
		static struct option LONG_OPTIONS[] = {
			{   NULL,  0,0, 0 }
		};
		int opt_index;

		const int c = getopt_long( argc, argv, CHAR_OPTIONS, LONG_OPTIONS, &opt_index );

		if( -1 == c ) break;

		switch( c ) {

		case 'c':
			strcpy( _contig, optarg );
			break;

		case 's':
			start = atoi(optarg);
			if( start < 1 ) {
				fprintf( stderr, "sequence start (%d) is less than 1\n", start );
				exit(-1);
			}
			break;

		case 'e':
			end   = atoi(optarg);
			if( end < 1 ) {
				fprintf( stderr, "sequence end (%d) is less than 1\n", end );
				exit(-1);
			}
			break;

		case 'm':
			break;

		case 'v':
			_verbosity = atoi( optarg );
			break;

		case 'i':
			_complement = true;
			break;

		// When this file is slaved to e.g. a Python script it's easier to 
		// build command lines in which all options take arguments, so this
		// is just an alternative (non-boolean) way to specify complementation.
		case 'd':
			_complement = atoi(optarg) < 0; // alternatively, optarg[0]=='-'
			break;

		case 'w':
			_o_width = atoi(optarg);
			break;

		case 'l':
			_linelen = atoi(optarg);
			if( _linelen < 1 ) {
				fprintf( stderr, "line length hint (%d) is less than 1\n", _linelen );
				exit(-1);
			}
			break;

		// Next two options are used to specify an output other than stdout.
		case 'a':
			_append = true;
			// Yes, fallthrough!
		case 'o':
			strcpy( oname, optarg );
			break;

		case 'S':
			suffix = optarg;
			break;

		case 'F':
			_fewer_ok = true;
			break;

		case 'h':
			print_usage( argv[0], stderr );
			exit(0);
			break;

		default:
			printf ("error: unknown option: %c\n", c );
			exit(-1);
		}
	} while(true);

	argi = optind;
	
	if( start < 1 || end < 1 ) {
		fprintf( stderr, "expected 1-based, fully-closed coordinates; received start=%d, end=%d.\n", start, end );
		exit(-1);
	}

	if( _complement )
		_emit = _fput_complement;

	/**
	  * Insure for the purposes of extract start < end, but note their
	  * order, since 'end < start' implies user wants sequence reversal.
	  */

	lb = __min( start, end );
	ub = __max( start, end );
	N = ub - lb + 1;

	obuf = calloc( N, sizeof(char) );

	ofp = strlen(oname) > 0 ? fopen( oname, _append ? "a" : "w" ) : stdout;

	if( NULL == ofp )
		perror( oname );

	if( argi < argc ) {
		fasta_file_name = argv[ argi ];
		ifp = fopen( fasta_file_name, "r" );
		if( NULL == ifp )
			perror( fasta_file_name );
	}

	/**
	 * Try to identify the position in the (possibly multi-sequence) file
	 * at which the DNA bases of the contig of interest begins--that is,
	 * the first line AFTER the header.
	 */

	if( ifp != NULL && obuf != NULL ) {

		struct _indexrec info = {
			.seqlen    = 0,
			.byte_offset = 0L,
			.linechars = _linelen,
			.linebytes = _linelen + NL_LEN
		};
		info.contig[0] = 0;

		// Attempt to use an index with a default name (<fasta_file_name>.fai), and
		// fall back to a linear scan if it's not available.
		// NOTE: Could fall back to just-in-time creation of index, but trying not
		// to reinvent more than I already am here...

		if( fasta_file_name != NULL
				&& strlen( fasta_file_name ) > 0
				&& lookup( fasta_file_name, _contig, 1 /* quiet */, &info ) == 0 ) {
			if( fseek( ifp, info.byte_offset, SEEK_SET ) != 0 ) {
				fprintf( stderr, "did not find %s in index of %s.\n", _contig, fasta_file_name );
				goto leave;
			}
		} else
			_find( ifp, strlen(_contig) > 0 ? _contig : ">" /* to insure first seq is used. */ );
	}

	/**
	  * If we have open input and output files, find the sequence and excise
	  * the interval.
	  */

	if( ifp != NULL && obuf != NULL ) {

		struct _indexrec info = {
			.seqlen    = 0,
			.byte_offset = 0L,
			.linechars = _linelen,
			.linebytes = _linelen + NL_LEN
		};
		info.contig[0] = 0;

		// Attempt to use an index with a default name (<fasta_file_name>.fai), and
		// fall back to a linear scan if it's not available.
		// NOTE: Could fall back to just-in-time creation of index, but trying not
		// to reinvent more than I already am here...

		if( fasta_file_name != NULL
				&& strlen( fasta_file_name ) > 0
				&& lookup( fasta_file_name, _contig, 1 /* quiet */, &info ) == 0 ) {
			if( fseek( ifp, info.byte_offset, SEEK_SET ) != 0 ) {
				fprintf( stderr, "did not find %s in index of %s.\n", _contig, fasta_file_name );
				goto leave;
			}
		} else
			_find( ifp, strlen(_contig) > 0 ? _contig : ">" /* to insure first seq is used. */ );

		if( ftell(ifp) > 0 /* since sequence always FOLLOWS header */ )
			n = excise( ifp, lb, ub, info.linechars, info.linebytes, obuf, N );
	}

	if( ifp )
		fclose( ifp );

	/**
	  * Dump out the buffer.
	  */

	if( interval_stream_fp ) {
		while() {
		}
		fclose( interval_stream_fp );
	} else {
	}

	if( n > 0 && ( n == N || _fewer_ok ) ) { // ...only possible if ofp != NULL above.

		// Emit a header containing the input FASTA sequence's header plus an
		// excision spec and any suffix specified on the command line.

		exit_status = EXIT_SUCCESS;
	}

leave:

	if( ofp )
		fclose( ofp );
	if( obuf )
		free( obuf );

	return exit_status;
}

