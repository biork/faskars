
# Read a BED file (6-column) and excise (using RK's fasta utility)
# flanks of the specified size.

if [ -z $1 ] || [ -z $2 ] || [ -z $3 ]; then
	echo "flanks.sh <dir> <wid> <len>"
	echo "Read the script for details."
	exit
fi

DIR="$1" # containing subject sequences
WID="$2" # character width of input FASTA file's data blocks
LEN="$3" # desired length of flanks

# Remember, BED files use 0-based half-open intervals, but
# fasta expects 1-based fully closed intervals. So...
# [x, y) in the bedfile should be converted to [x+1,y].
# For example,
#       [9,12)        BED file
#      [10,12]        FASTA coordinates of interval
#  [7,9]     [13,15]  FASTA coordinates of 3-bp flanks.

while read chromo start stop name score strand; do
	if [ $strand = "+" ]; then
		fasta -t "u:$name" -v 1 -l $WID    -s $(($start - ($LEN-1))) -e    $start        "$DIR/$chromo.fa"
		fasta -t "d:$name" -v 1 -l $WID    -s $(($stop+1))        -e $(($stop + $LEN))   "$DIR/$chromo.fa"
	else
		fasta -t "u:$name" -v 1 -l $WID -c -s $(($stop + $LEN)) -e $(($stop + 1))        "$DIR/$chromo.fa"
		fasta -t "d:$name" -v 1 -l $WID -c -s    $start        -e $(($start - ($LEN-1))) "$DIR/$chromo.fa"
	fi
done

