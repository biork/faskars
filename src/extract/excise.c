
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>  // for perror
#include <assert.h>

/**
  * Return WITHOUT ANY FORMATTING the single stretch of nucleotide sequence
  * specified by the 1-BASED and FULLY CLOSED [start,end] arguments, wherein
	* start and end are DNA sequence, not byte, coordinates.
  *
  * This function assumes that fp is positioned at the beginning of the DNA
  * sequence from which the excision is sought--that is, at the beginning of
	* the first line AFTER the header in a properly-formatted FASTA file.
  * (How it got there depends on whether this is called from the main of the
  * standalone program or the C interface of the Python package.)
  *
  * The bytelen argument is an entirely optional optimization that the
  * caller provides as a hint. If provided, it allows an fseek to short-
  * circuit what is otherwise an inefficient linear scan.
  *
  * Returns a negative number on error, otherwise the number of 
  * sequence characters actually copied.
	* The returned sequence will contain NO WHITESPACE at all NOR is it NUL-
	* terminated.
  */

#define EFAIL (-1)

int excise( FILE *fp,
		unsigned int start,
		unsigned int end,
		unsigned int charlen,
		unsigned int bytelen,
		char *obuf, size_t buflen ) {

	char * const BUFFER_END = obuf + buflen;

	char c, *pc = obuf;
	int n = 0;

	assert( start <= end );

	// If a line length has been specified, skip forward to that point of the
	// file, updating all relevant variables.

	if( bytelen > 0 ) {

		// Consume enough whole lines to position outselves at the beginning of
		// the line containing the actual start position.

		const int NLINES = (start-1) / charlen;

		if( NLINES > 0 ) {

			// Make sure the fseek accounts for the (platform-dependent) size of 
			// the line terminator...

			if( fseek( fp, NLINES*bytelen, SEEK_CUR ) < 0 ) {
				perror("fseek'ing to start" );
				return -EFAIL;
			}

			// ...but the line terminator does not enter into character consumption
			// bookkeeping.

			n += (NLINES*charlen);
		}
	}

	// fp should now be positioned at the start of the FASTA file line
	// containing the first character of interest.

	do {
		c = fgetc( fp );
		if( EOF == c )
			return -EFAIL;
		else
		if( isalpha(c) ) 
			n++; // ...one of [acgtACGTnN*], actually.
		else
		if( c == '>' ) {
			fprintf( stderr,
				"encountered next sequence in FASTA file before basepair %d in desired sequence.", start );
			return -EFAIL;
		}
	} while( n < start );

	/**
	  * Once we've loaded at least <start> characters, begin echoing them
	  * to the output buffer up through and including the <end>th char.
		* Notice that we emit first. If we're emitting [3,5] from
		* >foo
		* ACGTTGCA
		*
		* ...then c == 'G' already. 'G' was loaded above.
	  */

	while( EOF != c && n <= end && c != '>' && pc < BUFFER_END ) {
		if( isalpha(c) /* in other words, ignore NL */ ) {
			*pc++ = c;
			n++; // ...one of [acgtACGTnN*], actually.
		}
		c = fgetc( fp );
	}

	return pc - obuf;
}

