
for start in $(seq -f $'%.0f' 1 1000); do 
	end=$((start + 100))
	echo -n "$start $end "
	./fasta64 -v 1 -s $start -e $end       ~/data/ncbi/genomes/M_musculus/Assembled_chromosomes/fromChIPseqFolder/chr1.fa > a
	./fasta64 -v 1 -s $start -e $end -l 50 ~/data/ncbi/genomes/M_musculus/Assembled_chromosomes/fromChIPseqFolder/chr1.fa > b
	diff -s a b
done

for start in $(seq -f $'%.0f' 1 1000); do 
	end=$((start + 100))
	echo -n "$start $end "
	./fasta64 -c -v 1 -s $start -e $end       ~/data/ncbi/genomes/M_musculus/Assembled_chromosomes/fromChIPseqFolder/chr1.fa > a
	./fasta64 -c -v 1 -s $start -e $end -l 50 ~/data/ncbi/genomes/M_musculus/Assembled_chromosomes/fromChIPseqFolder/chr1.fa > b
	diff -s a b
done

