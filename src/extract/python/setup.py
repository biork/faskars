from distutils.core import setup,Extension

setup(name="fasta",
	version="0.1",
	description="Extraction of sequence from FASTA files",
	long_description="""\
	Extraction of sequence from multi-sequence FASTA files
	""",
	author="Roger Kramer",
	author_email="rkramer@systemsbiology.org",
	url="www.systemsbiology.org",
#	py_modules=['fasta'],
	packages=[
		'fasta'],
#	package_dir=
#	package_data={'fasta':['template.html','template.css','render.js']},
	ext_package='fasta', # ...scopes all extensions
	ext_modules=[
		Extension('compiled',
			['c/excise.c',
			 'c/module.c',
			],
			extra_compile_args=['-std=c99'],
			define_macros=[("NL_LEN","1"),])]
#	data_files=[('.bdqc',['data/plugins.txt',]),]
	)


