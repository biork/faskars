
"""
"""
from fasta.compiled import excise

def fetch( filename, start, end, width, complement ):
	"""
	Returns sequence from a single-sequence file.
	"""
	rev = end < start
	# This is the actual 
	s = min(start,end)
	e = max(start,end)
	b = bytearray(E-S+1)
	status = excise( filename,  s, e, width, b )
	seq = b.decode()
	return reversed(seq) if ref else seq


def Db(object):
	"""
	When you want to extract sequence by m 
	"""

	@staticmethod
	def load_index( fp ):
		"""
		Returns a dict mapping accessions to
		(file,header_offset,seq_offset) triples
		"""
		fname = ''
		index = {}
		line = fp.readline()
		while len(line) > 0:
			if line.startswith("@"):
				fname = line[1:].rstrip()
			else:
				line.rstrip().split('\t')
				index[ f[0] ] = (fname, int(f[1]), int(f[2]))
				# Let any exceptions from above bubble up!
			line = fp.readline()
		return index

	def __init__( self, index, root=None ):
		with open(index) as fp:
			self.index = Db.load_index( fp )
		self.root = root if root is not None else CWD

	def fetch( self, accession, start, end, complement=False ):
		"""
		Return as a single undelimited string the given 1-based,
		closed range of nucleotides from the sequence specified
		by the given accession.
		"""
		location = self.index[ accession ]
		with open( location[0] ) as fp:
			fp.seek( location[2] )

#from fasta.compiled import excise

if __name__=="__main__":
	import sys
	print( fetch( *sys.argv[1:] ) )

