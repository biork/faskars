
#include <Python.h>

//static PyObject *FastaError;
extern int excise( FILE *fp, int start, int end, int linelen /* !!! EXCLUDING NL !!! */, char *obuf );

// forward decl
static PyObject *
fasta_excise(PyObject *self, PyObject *args);

static PyMethodDef _methods[] = {
    {"excise",  fasta_excise, METH_VARARGS,
     "Remove a stretch of sequence by (1-based) coordinate bounds"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef _moduledef = {
   PyModuleDef_HEAD_INIT,
   "compiled",   /* name of module */
   "some usage docs",
   -1,       /* size of per-interpreter state of the module,
                or -1 if the module keeps state in global variables. */
   _methods
};


static PyObject *
fasta_excise( PyObject *self, PyObject *args ) {

	FILE *fp = NULL;
    const char *filename = NULL;
	PyObject *retval = NULL;
	unsigned int s, e, w;
	Py_buffer b;

    if( ! PyArg_ParseTuple( args, "sIIIy*", &filename, &s, &e, &w, &b ) )
        return NULL;

	strcpy( b.buf, "Hello" );
	printf( "%s\t%d\t%d\t%d\n", filename, s, e, w );
	fp = fopen( filename, "r" );
	if( fp ) {

		fclose( fp );
		//const long STATUS = excise( fp, s, e, w, buf );
	}
	PyBuffer_Release( &b );
/*
				PyErr_SetFromErrnoWithFilename( PyExc_IOError, filename );
				retval = PyLong_FromLong( -e.ordinal );
				retval = PyLong_FromLong( +e.ordinal );
				PyErr_SetString( PyExc_RuntimeError,
					"unknown error/unfinished code in " __FILE__ );
		PyErr_SetFromErrnoWithFilename( PyExc_IOError, filename );
*/
	retval = PyLong_FromLong( 0 );
    return retval;
}


PyMODINIT_FUNC PyInit_compiled(void) {

    PyObject *m
    	= PyModule_Create( &_moduledef );
    if (m == NULL)
        return NULL;
/*
    FastaError = PyErr_NewException("text.error", NULL, NULL);
    Py_INCREF(FastaError);
    PyModule_AddObject(m, "error", FastaError);
*/
    return m;
}

