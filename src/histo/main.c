
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <assert.h>

int main( int argc, char *argv[] ) {

	uint64_t total = 0; 
	uint64_t table[256];
	assert( sizeof(table) == sizeof(uint64_t)*256 );
	memset( table, 0, sizeof(table) );
	while( 1 ) {
		const int C = getchar();
		if( C < 0 )
			break;
		else {
			table[ C ] += 1;
			total++;
		}
	}
	for(int i = 0; i < 256; i++ ) {
		printf( "%d\t%c\t%ld\n", i, isgraph(i) ? i : '.', table[i] );
	}
	printf( "# %ld\n", total );
	return EXIT_SUCCESS;
}

